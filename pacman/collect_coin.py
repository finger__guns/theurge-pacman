from typing import List


def collect_coin(coin_count: int, coins: List[str], current_x, current_y) -> int:
    # So we have to return the coin count as an integer and only collect coins that haven't been so before.
    # so lets just put the collected coins in the coins list and that will be that.
    # to start we can use the same little location f string
    _location_string = f'{current_x}:{current_y}'
    if _location_string not in coins:
        coin_count += 1
        # and then append that coin location to the list!
        coins.append(_location_string)
        return coin_count
    else:
        # And if the coins already in the list act as if nothing happend
        return coin_count
