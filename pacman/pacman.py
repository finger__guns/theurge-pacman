# Just for nice error messages
import logging
import sys
from typing import Dict

from prop_move import proposed_move
from valid_move import valid_move
from collect_coin import collect_coin

# First thing is to read the directions from the file
def input_values(file_name: str) -> Dict[str, int]:
    """ Return a dictionary of string and intger values """
    with open(file_name, 'r') as f:
        # Lets have it as dynamic as can be - assume the first line is always going to be the dimensions of the board.
        # SO! I'm not gonna use pythons typing for these next little bits, they are gonna change a whole bunch real quick so that would be a waste of compute time.
        # Would look pretty though, but I will annotate them just so ya know I'm not a potato.
        board_dimensions = f.readline() # array at this point
        # Get x and y from that initial read.
        board_x, board_y = board_dimensions.split() # X and Y become strings
        # Convert those x y cords to int for sane purpose.
        board_x = int(board_x) # Now an int
        board_y = int(board_y) # Now an int
        # logging.debug(f"x is {board_x}")
        # logging.debug(f"y is {board_y}")

        # The next little bit is getting the initial position of pacman himself.
        pacman_init = f.readline() # Same as above for the typing.
        # should probably just zip the lines and assign variables that way ????
        pacman_init_x, pacman_init_y = pacman_init.split()
        # Again convert these to integer for later
        pacman_init_x = int(pacman_init_x)
        pacman_init_y = int(pacman_init_y)

        # STUPID MOVEMENTS THAT I FORGOT ABOUT FOR A SECOND
        # movements = f.readline() Could have sworn that was an array ?????
        movements = []
        movements = f.readline().strip('\n')

        # and then the whole wall thing.
        walls = f.read().splitlines() # Gotta do list comp so ignore this really.
        walls = [space.replace(' ', ':') for space in walls]

        # Now we can just return a dictionary of all this collected jazz.
        return { 'board_x': board_x, 'board_y': board_y,
                 'pacman_init_x': pacman_init_x, 'pacman_init_y' : pacman_init_y,
                 'movements': movements,
                 'walls': walls }


def pacman(input_file: str) -> str:
    """ take the passed in file and collect coins. return the current position both x,y and the coin count. """

    # Assign the dictionary to a varaible so we aren't reading the file every time.
    inputs = input_values(input_file)
    # Get the first x and y cords.
    current_x = inputs['pacman_init_x']
    current_y = inputs['pacman_init_y']

    # Create a location_string
    _location_string = f"{current_x}:{current_y}"

    # Lets initiailize the coin count and create a list
    coin_count = 0
    coins = []
    coins.append(_location_string)


    # So now we know there is a 'coin' at this init location
    # Lets loop over the movements, make sure they aren't stupid and get more coins.
    # This is where naming things in a dictionary as best you can is best.
    move_list = inputs['movements']
    for move in move_list:
        # Now lets check if they are legit moves
        proposed_x, proposed_y = proposed_move(current_x, current_y, move)
        if valid_move(inputs, proposed_x, proposed_y):
            # So the move is valid
            current_x = proposed_x
            current_y = proposed_y
            # Collect coins
            coin_count = collect_coin(coin_count, coins, current_x, current_y) # Again smart variable names means I can just autocomplete this whole function.
        return f"{current_x} {current_y} {coin_count}"

if __name__ == '__main__':
    input_file = sys.argv[1]
    print(pacman(input_file))
