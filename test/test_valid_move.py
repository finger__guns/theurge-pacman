import unittest
from pacman.valid_move import valid_move


class ValidMoveTest(unittest.TestCase):
    def test_x_zero_y_one(self):
        # Dictionary to test the whole format string thing
        values = { 'board_x': 5, 'board_y': 5,
                   'pacman_init_x': 0, 'pacman_init_y' : 1,
                   'movements': 'NNESEESWNWW',
                   # Just fake the whole formated string thing
                   'walls': '1:1' }
        self.assertTrue(valid_move(values, 0, 1))

    def test_x_one_y_zero(self):
        # Dictionary to test the whole format string thing
        values = { 'board_x': 5, 'board_y': 5,
                   'pacman_init_x': 0, 'pacman_init_y' : 1,
                   'movements': 'NNESEESWNWW',
                   # Just fake the whole formated string thing
                   'walls': '1:1' }
        self.assertTrue(valid_move(values, 1, 1))

    def test_x_neg_one_y_one(self):
        # Should complain
        # Dictionary to test the whole format string thing
        values = { 'board_x': 5, 'board_y': 5,
                   'pacman_init_x': 0, 'pacman_init_y' : 1,
                   'movements': 'NNESEESWNWW',
                   # Just fake the whole formated string thing
                   'walls': '0:1' }
        self.assertFalse(valid_move(values, -1, 1))
