def valid_move(input_values, x: int, y: int) -> bool:
    # Read the board dimensions from the input dict which is kinda easy enough, if I was gonna re do this I'd go oop style rather than functional.
    # It's times like these I wish I could use ruby and its little ? in function names and what not, luckily we can use pythons types I guess thats better.
    _location_string = f'{x}:{y}' # Super happy python has f strings now, no more stupid str(x) - still has its place but this is just super easy.
    # Pretty sure an if any x in y type thing to go over the walls and board dimensions will be nicest here.
    if any(_location_string in wall for wall in input_values['walls']):
        return False # We can't move through walls.
    # Now to check 0 and board_x and y
    elif (x < 0) or (x >= input_values['board_x']):
        return False
    # Same for Y
    elif (y < 0) or (y >= input_values['board_y']):
        return False
    else:
        # Its gotta be a good move
        return True
