import unittest
from pacman.prop_move import proposed_move


class ProposedMoveTest(unittest.TestCase):
    def test_proposed_move_n(self):
        """ If we move north from 1 1 there should be 1 2"""
        x, y = proposed_move(1, 1, 'N')
        self.assertEquals(y, 2)

    def test_proposed_move_s(self):
        """ If there is a move south from 1 1 it will be 1 0 """
        x, y = proposed_move(1, 1, 'S')
        self.assertEquals(y, 0)

    def test_proposed_move_e(self):
        """ An east move will be 2 1"""
        x, y = proposed_move(1, 1, 'E')
        self.assertEquals(x, 2)

    def test_proposed_move_w(self):
        """ A west move will be 0 1"""
        x, y = proposed_move(1, 1, 'W')
        self.assertEquals(x, 0)
