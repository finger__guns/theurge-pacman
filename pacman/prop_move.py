from typing import Tuple


def proposed_move(current_x: int, current_y: int, _proposed_move: str) -> Tuple[str, str]:
    # Lets just get a whole bunch of proposed move and return what would happen.
    # Behold my if statements

    # Gotta give temporary value to not overwrite itself, this is pirmarly for testing.
    proposed_move_x = current_x
    proposed_move_y = current_y

    # So these movements will all be itsself plus or minus 1
    if _proposed_move == 'N':
        proposed_move_y = proposed_move_y +1
    elif _proposed_move == 'S':
        proposed_move_y = proposed_move_y - 1
    elif _proposed_move == 'E':
        proposed_move_x = proposed_move_x + 1
    elif _proposed_move == 'W':
        proposed_move_x = proposed_move_x - 1
    # Just return them in the same order they are passed in.
    return (proposed_move_x, proposed_move_y)
