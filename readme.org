* Pacman
  Just move pacman from a file of directions.
  Everything is done in the whole pipenv thing, this does make use (where it makes sense to) of [[https://www.python.org/dev/peps/pep-0484/][pythons typing pep]].
  Things are seperated and tested for the most part even if they are just little asserts.
  Coins get collected and things get printed.
